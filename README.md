# google_calendar_freebusy_demo #
Simple demo of google's oauth2 workflow and authorized queries to the google calendar API with use of the freebusy query method.

## Setup & execution ##
    git clone git@bitbucket.org:aisle51/google-calendar-integration.git
    cd google-calendar-integration
    pip3 install -r ./requirements.txt
    python3 ./google_calendar_freebusy_demo.py
    <navigate to "localhost:5000" in a browser>
    
Currently the app only prints the user's freebusy data to the console and serves a
text representation, as a working example.


## Notes ##
* **Pros with multiple google accounts:** Currently the oauth2 workflow I used only allows for the selection of one Google account's calendars. If the pro has more than one account that they want the calendars considered from, behavior will need to be more complex and I didn't discover any convenient workflows for that case.
* **Multiple calenders per google account:** Google accounts often have more than one calendar and the pro may want to only have only one or specific calendars considered for automatic sceduling. The demo simply gets free/busy data for all calendars in a specific google account.
* **Auto generating a google serviz calendar per pro:** This would simplify things on our end as we wouldn't necessarilly need to store anything about the pro's schedule outself. The pro's serviz specific calendar would simply be stored as one of the pro's google calendars which the user could change either through google's interface or ours. Or something like that. Of course polling and updates become a little more complicated.
* **Calendar groups:** There is also the concept of calendar groups in google, obviously for grouping related calendars, which we may want to consider support for at some point. I haven't looked into it much.