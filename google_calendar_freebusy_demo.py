import os
import pprint
import pytz
import httplib2
from oauth2client.client import flow_from_clientsecrets
from apiclient.discovery import build
from flask import Flask, redirect, request
from datetime import datetime
from dateutil.relativedelta import relativedelta

SCOPE = 'https://www.googleapis.com/auth/calendar'
REDIRECT_URI = 'http://localhost:5000/app'

# get client secrets from file
APP_DIR = os.path.dirname(os.path.abspath(__file__))
CLIENT_SECRETS = None
for f in os.listdir(APP_DIR):
    if f.startswith('client_secret_') and f.endswith('.apps.googleusercontent.com.json'):
        CLIENT_SECRETS = os.path.join(APP_DIR, f)

app = Flask(__name__)


@app.route('/')
def login():

    # authorize google calendar access from user to application
    flow = flow_from_clientsecrets(CLIENT_SECRETS, scope=SCOPE, redirect_uri=REDIRECT_URI)
    auth_uri = flow.step1_get_authorize_url()
    response = redirect(auth_uri, code=302)
    return response


@app.route('/app')
def application():

    # build service for authorized calendar queries
    flow = flow_from_clientsecrets(CLIENT_SECRETS, scope=SCOPE, redirect_uri=REDIRECT_URI)
    credentials = flow.step2_exchange(request.args.get('code'))
    http = httplib2.Http()
    http = credentials.authorize(http)
    service = build('calendar', 'v3', http=http)

    # get all of the user's calendars
    page_token = None
    calendars = []
    while True:
        calendar_list = service.calendarList().list(pageToken=page_token).execute()
        for calendar in calendar_list['items']:
            calendars.append({'id': calendar['id']})
        page_token = calendar_list.get('nextPageToken')
        if not page_token:
            break

    # query freebusy data from all of the user's calendars
    tz = pytz.timezone('UTC')
    now = tz.localize(datetime.utcnow())
    then = now + relativedelta(months=3)
    query = {'timeMin': now.strftime('%Y-%m-%dT%H:%M:%S%z'),
             'timeMax': then.strftime('%Y-%m-%dT%H:%M:%S%z'),
             'items': calendars}
    r = service.freebusy().query(body=query)
    results = r.execute()
    pprint.pprint(results)
    return pprint.pformat(results)
    # YAY! :)

if __name__ == '__main__':
    app.run()
